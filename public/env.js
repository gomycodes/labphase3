window.env = {
  "LESSOPEN": "| /usr/bin/lesspipe %s",
  "USER": "aikpe",
  "npm_config_user_agent": "npm/8.5.0 node/v16.14.2 linux x64 workspaces/false",
  "SSH_AGENT_PID": "1658",
  "XDG_SESSION_TYPE": "x11",
  "npm_node_execpath": "/home/aikpe/.nvm/versions/node/v16.14.2/bin/node",
  "SHLVL": "1",
  "npm_config_noproxy": "",
  "HOME": "/home/aikpe",
  "DESKTOP_SESSION": "ubuntu",
  "NVM_BIN": "/home/aikpe/.nvm/versions/node/v16.14.2/bin",
  "npm_package_json": "/home/aikpe/Desktop/Gomycode/labphase3/package.json",
  "NVM_INC": "/home/aikpe/.nvm/versions/node/v16.14.2/include/node",
  "GNOME_SHELL_SESSION_MODE": "ubuntu",
  "GTK_MODULES": "gail:atk-bridge",
  "MANAGERPID": "1478",
  "npm_config_userconfig": "/home/aikpe/.npmrc",
  "npm_config_local_prefix": "/home/aikpe/Desktop/Gomycode/labphase3",
  "DBUS_STARTER_BUS_TYPE": "session",
  "DBUS_SESSION_BUS_ADDRESS": "unix:path=/run/user/1000/bus,guid=f5dfc25a2802b12c16fe929562c2cdec",
  "COLORTERM": "truecolor",
  "COLOR": "1",
  "NVM_DIR": "/home/aikpe/.nvm",
  "npm_config_metrics_registry": "https://registry.npmjs.org/",
  "MANDATORY_PATH": "/usr/share/gconf/ubuntu.mandatory.path",
  "IM_CONFIG_PHASE": "1",
  "LOGNAME": "aikpe",
  "JOURNAL_STREAM": "8:46631",
  "_": "/home/aikpe/.nvm/versions/node/v16.14.2/bin/npm",
  "npm_config_prefix": "/home/aikpe/.nvm/versions/node/v16.14.2",
  "XDG_SESSION_CLASS": "user",
  "DEFAULTS_PATH": "/usr/share/gconf/ubuntu.default.path",
  "USERNAME": "aikpe",
  "TERM": "xterm-256color",
  "npm_config_cache": "/home/aikpe/.npm",
  "GNOME_DESKTOP_SESSION_ID": "this-is-deprecated",
  "WINDOWPATH": "2",
  "npm_config_node_gyp": "/home/aikpe/.nvm/versions/node/v16.14.2/lib/node_modules/npm/node_modules/node-gyp/bin/node-gyp.js",
  "PATH": "/home/aikpe/Desktop/Gomycode/labphase3/node_modules/.bin:/home/aikpe/Desktop/Gomycode/node_modules/.bin:/home/aikpe/Desktop/node_modules/.bin:/home/aikpe/node_modules/.bin:/home/node_modules/.bin:/node_modules/.bin:/home/aikpe/.nvm/versions/node/v16.14.2/lib/node_modules/npm/node_modules/@npmcli/run-script/lib/node-gyp-bin:/home/aikpe/.nvm/versions/node/v16.14.2/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin",
  "SESSION_MANAGER": "local/aikpe:@/tmp/.ICE-unix/1696,unix/aikpe:/tmp/.ICE-unix/1696",
  "INVOCATION_ID": "a1be1905645447969946b795f9ca55f3",
  "NODE": "/home/aikpe/.nvm/versions/node/v16.14.2/bin/node",
  "npm_package_name": "labphase3",
  "XDG_MENU_PREFIX": "gnome-",
  "GNOME_TERMINAL_SCREEN": "/org/gnome/Terminal/screen/c47bc37e_1444_4db9_ab69_61791b0ebd72",
  "XDG_RUNTIME_DIR": "/run/user/1000",
  "DISPLAY": ":0",
  "LANG": "en_US.UTF-8",
  "XDG_CURRENT_DESKTOP": "ubuntu:GNOME",
  "XMODIFIERS": "@im=ibus",
  "XDG_SESSION_DESKTOP": "ubuntu",
  "XAUTHORITY": "/run/user/1000/gdm/Xauthority",
  "LS_COLORS": "rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=00:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.zst=01;31:*.tzst=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.wim=01;31:*.swm=01;31:*.dwm=01;31:*.esd=01;31:*.jpg=01;35:*.jpeg=01;35:*.mjpg=01;35:*.mjpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.m4a=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.oga=00;36:*.opus=00;36:*.spx=00;36:*.xspf=00;36:",
  "GNOME_TERMINAL_SERVICE": ":1.90",
  "npm_lifecycle_script": "react-dotenv && react-scripts start",
  "SSH_AUTH_SOCK": "/run/user/1000/keyring/ssh",
  "SHELL": "/bin/bash",
  "npm_package_version": "0.1.0",
  "npm_lifecycle_event": "start",
  "QT_ACCESSIBILITY": "1",
  "GDMSESSION": "ubuntu",
  "LESSCLOSE": "/usr/bin/lesspipe %s %s",
  "GPG_AGENT_INFO": "/run/user/1000/gnupg/S.gpg-agent:0:1",
  "QT_IM_MODULE": "ibus",
  "npm_config_globalconfig": "/home/aikpe/.nvm/versions/node/v16.14.2/etc/npmrc",
  "npm_config_init_module": "/home/aikpe/.npm-init.js",
  "PWD": "/home/aikpe/Desktop/Gomycode/labphase3",
  "npm_execpath": "/home/aikpe/.nvm/versions/node/v16.14.2/lib/node_modules/npm/bin/npm-cli.js",
  "XDG_CONFIG_DIRS": "/etc/xdg/xdg-ubuntu:/etc/xdg",
  "NVM_CD_FLAGS": "",
  "DBUS_STARTER_ADDRESS": "unix:path=/run/user/1000/bus,guid=f5dfc25a2802b12c16fe929562c2cdec",
  "XDG_DATA_DIRS": "/usr/share/ubuntu:/usr/local/share/:/usr/share/:/var/lib/snapd/desktop",
  "npm_config_global_prefix": "/home/aikpe/.nvm/versions/node/v16.14.2",
  "npm_command": "start",
  "VTE_VERSION": "6003",
  "INIT_CWD": "/home/aikpe/Desktop/Gomycode/labphase3",
  "EDITOR": "vi",
  "API_URL": "http://localhost:4000"
};