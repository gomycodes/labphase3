import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import Row from 'react-bootstrap/Row';
import {Formik} from 'formik';
import * as yup from 'yup';
import { useState, useEffect } from 'react';
import axios from 'axios';
import env from "react-dotenv";

const schema = yup.object().shape({
  name: yup.string().required(),
  price: yup.number().required(),
  description: yup.string().required(),
  category: yup.string().required(),
  image: yup.mixed().required(),
});

export default function AddProduct() {

  const [categories, setCategories] = useState([]);

  useEffect(() => {
    axios.get(`${env.API_URL}/categories`)
    .then(response => {
      if(response.status === 200){
        setCategories(response.data)
      }
    })
    .catch(err => console.log(err))
  }, []);
  const handleSubmit = (e)=>{
    const formData = {};

    if(e.target.name.value && e.target.description.value){
      formData['name'] = e.target.name.value;
      formData['price'] = e.target.price.value;
      formData['description'] = e.target.description.value;
      formData['category'] = e.target.category.value;
      console.log(formData)
      axios.post(`${env.API_URL}/products`, formData).then(response =>{
       e.target.name.value = "";
       e.target.price.value = "";
       e.target.description.value = "";
       e.target.category.value = "";
      })
    }


    e.preventDefault();
  }

  return (
    <Formik
      validationSchema={schema}
      initialValues={{
        name: '',
        price: 0,
        description: '',
        category: '',
        image: null,
      }}
    >
      {({
        handleChange,
        handleBlur,
        values,
        touched,
        isValid,
        errors,
      }) => (
        <Form noValidate onSubmit={handleSubmit}>
          <Row className="mb-3">
            <Form.Group
              as={Col}
              md="4"
              controlId="validationFormik101"
              className="position-relative"
            >
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="text"
                name="name"
                value={values.name}
                onChange={handleChange}
                isValid={touched.name && !errors.name}
              />
              <Form.Control.Feedback tooltip>Looks good!</Form.Control.Feedback>
            </Form.Group>
            <Form.Group
              as={Col}
              md="8"
              controlId="validationFormik102"
              className="position-relative"
            >
              <Form.Label>Description</Form.Label>
              <Form.Control
                type="text"
                name="description"
                value={values.description}
                onChange={handleChange}
                isValid={touched.description && !errors.description}
              />

              <Form.Control.Feedback tooltip>Looks good!</Form.Control.Feedback>
            </Form.Group>
          </Row>
          
          <Row className="mb-3">
          <Form.Group as={Col} md="4" controlId="validationFormikUsername2">
              <Form.Label>Price</Form.Label>
              <InputGroup hasValidation>
                <InputGroup.Text id="inputGroupPrepend">$</InputGroup.Text>
                <Form.Control
                  type="number"
                  placeholder="price"
                  aria-describedby="inputGroupPrepend"
                  name="price"
                  value={values.price}
                  onChange={handleChange}
                  isInvalid={!!errors.price}
                />
                <Form.Control.Feedback type="invalid" tooltip>
                  {errors.price}
                </Form.Control.Feedback>
              </InputGroup>
            </Form.Group>

            <Form.Group as={Col} md="4" className="position-relative mb-3">
            <Form.Label>Category</Form.Label>
            
            <Form.Select name="category" aria-label="Default select example" onChange={handleChange}
              >
               <option>Open this select menu</option>
              {
                categories.map(category => (<option key={category._id} value={category._id}>{category.name}</option>))
              }
            </Form.Select>
          </Form.Group>

            <Form.Group as={Col} md="4" className="position-relative mb-3">
            <Form.Label>Image</Form.Label>
            <Form.Control
              type="file"
              required
              name="image"
              onChange={handleChange}
              isInvalid={!!errors.image}
            />
            <Form.Control.Feedback type="invalid" tooltip>
              {errors.image}
            </Form.Control.Feedback>
          </Form.Group>
          </Row>

          <Button type="submit">Submit form</Button>
        </Form>
      )}
    </Formik>
  );
}
