import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import Row from 'react-bootstrap/Row';
import {Formik} from 'formik';
import * as yup from 'yup';
import { useState, useEffect } from 'react';
import axios from 'axios';
import env from "react-dotenv";

const schema = yup.object().shape({
  name: yup.string().required(),
  description: yup.string().required(),
});

export default function AddCategory() {

  const handleSubmit = (e)=>{
    const formData = {};
    if(e.target.name.value && e.target.description.value){
      formData['name'] = e.target.name.value;
      formData['description'] = e.target.description.value;
      axios.post(`${env.API_URL}/categories`, formData).then(response =>{
        e.target.description.value = "";
        e.target.name.value = ""
      })
    }
    e.preventDefault();
  }

  return (
    <Formik
      validationSchema={schema}
      onSubmit={console.log}
      initialValues={{
        name: '',
        description: '',
      }}
    >
      {({
        handleChange,
        handleBlur,
        values,
        touched,
        isValid,
        errors,
      }) => (
        <Form noValidate onSubmit={handleSubmit}>
          <Row className="mb-3">
            <Form.Group
              as={Col}
              md="4"
              controlId="validationFormik101"
              className="position-relative"
            >
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="text"
                name="name"
                value={values.name}
                onChange={handleChange}
                isValid={touched.name && !errors.name}
              />
              <Form.Control.Feedback tooltip>Looks good!</Form.Control.Feedback>
            </Form.Group>
            <Form.Group
              as={Col}
              md="8"
              controlId="validationFormik102"
              className="position-relative"
            >
              <Form.Label>Description</Form.Label>
              <Form.Control
                type="text"
                name="description"
                value={values.description}
                onChange={handleChange}
                isValid={touched.description && !errors.description}
              />

              <Form.Control.Feedback tooltip>Looks good!</Form.Control.Feedback>
            </Form.Group>
          </Row>
          <Button type="submit">Créer</Button>
        </Form>
      )}
    </Formik>
  );
}
