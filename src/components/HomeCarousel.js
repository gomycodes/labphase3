import Carousel from 'react-bootstrap/Carousel';
import img1 from '../imgs/img1.png';
import img2 from '../imgs/img2.png';
import img3 from '../imgs/img3.png';
import img4 from '../imgs/img4.png';
function HomeCarousel() {
  return (
    <Carousel variant="dark">
      <Carousel.Item style={{heigth: 200}}>
        <img
          className="d-block w-100"
          src={img1}
          alt="First slide"
        />
        <Carousel.Caption>
          <h5>Titre1</h5>
          <p>Description1.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src={img2}
          alt="Second slide"
        />
        <Carousel.Caption>
          <h5>Titre2</h5>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit2.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src={img3}
          alt="Third slide"
        />
        <Carousel.Caption>
          <h5>Titre3</h5>
          <p>
            Praesent commodo cursus magna, vel scelerisque nisl consectetur3.
          </p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  );
}

export default HomeCarousel;