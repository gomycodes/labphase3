import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import { useState, useEffect } from 'react';
import Product from './Product';
import axios from 'axios';
import env from "react-dotenv";

export default function ListProduct() {

  const [products, setProducts] = useState([]);
  useEffect(() => {
    axios.get(`${env.API_URL}/products`)
      .then(response =>{
        if(response.status === 200){
          setProducts(response.data);
        }
      })
  }, []);

  function deleteProduct(event,_id){
    event.preventDefault();
    axios.delete(`${env.API_URL}/products/${_id}`)
    .then(response =>{
      const productList = products.filter(product => product._id !== _id);
      setProducts(productList);
      console.log(response.status)
      // if(response.status === 200){
      //   const productList = products.filter(product => product._id !== product._id);
      //   setProducts(productList);
      // }
    })
  }

  return (
    <Table striped bordered hover>
      <thead>
        <tr>
          <th>#</th>
          <th>Nom</th>
          <th>Category</th>
          <th>Description</th>
          <th>Prix</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        {products.map((product,key) =>{
          
         return (
         <tr key={key}>
            <td>{key}</td>
            <td>{product.name}</td>
            <td>{product.category}</td>
            <td>{product.description}</td>
            <td>{product.price}$</td>
            <td>
              <Button variant="outline-success">Edit</Button>{' '}
              <Button variant="outline-danger" onClick={(event)=>deleteProduct(event, product._id)}>Delete</Button>{' '}
            </td>
          </tr>
        )
        })}
      </tbody>
    </Table>
  );
}
