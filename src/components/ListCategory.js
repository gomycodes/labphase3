import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';

import { useState, useEffect } from 'react';
import axios from 'axios';
import env from "react-dotenv";

export default function ListCategory() {

  const [show, setShow] = useState(false);
  const [name, setName] = useState('');
  const [id, setId] = useState('');

  const [description, setDescription] = useState('');
  const handleClose = () => setShow(false);

  const update = (e) => {

    e.preventDefault();
   axios.put(`${env.API_URL}/categories/${id}`,
   {
    name: name,
    description: description
  }).then(response =>{
    // const category = categories.filter(categorie => categorie._id !== _id);
    // setCategories(category)
    console.log(response)
   })
    setShow(false);
  }
  const handleShow = (e, categorie) => {
    setShow(true);
    setName(categorie.name)
    setDescription(categorie.description)
    setId(categorie._id);
  };

  const [categories, setCategories] = useState([]);
  useEffect(() => {
    axios.get(`${env.API_URL}/categories`)
    .then(response => {
      if(response.status === 200){
        setCategories(response.data)
      }
    })
    .catch(err => console.log(err))
  }, []);

  const deleteCategory =(e, _id) => {
    e.preventDefault();
   axios.delete(`${env.API_URL}/categories/${_id}`).then(response =>{
    const category = categories.filter(categorie => categorie._id !== _id);
    setCategories(category)
   })
  }

  const modal = (
    <Modal show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Editer Category#{name}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
            <Form.Label>Nom</Form.Label>
            <Form.Control
              type="text"
              placeholder=""
              value={name}
              onChange={(e)=>setName(e.target.value)}
              autoFocus
            />
          </Form.Group>
          <Form.Group
            className="mb-3"
            controlId="exampleForm.ControlTextarea1"
          >
            <Form.Label>Description</Form.Label>
            <Form.Control as="textarea" 
            onChange={(e)=>setDescription(e.target.value)}
            value={description} rows={3} />
          </Form.Group>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Fermer
        </Button>
        <Button variant="primary" onClick={update}>
          Modifier
        </Button>
      </Modal.Footer>
    </Modal>
)

  return (
    <>
    {modal}
    <Table striped bordered hover>
      <thead>
        <tr>
          <th>#</th>
          <th>Nom</th>
          <th>Description</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>

        {categories.map((categorie, key) =>{
          return (
          <tr key={key}>
            <td>{key} {categorie._id}</td>
            <td>{categorie.name}</td>
            <td>{categorie.description}</td>
            <td>
              <Button variant="outline-success" onClick={(e)=> handleShow(e, categorie)}>Edit</Button>{' '}
              <Button variant="outline-danger" onClick={(e)=> deleteCategory(e, categorie._id)}>Delete</Button>{' '}
            </td>
          </tr>)
        })}
        
      </tbody>
    </Table>
    </>
  );


 
}

