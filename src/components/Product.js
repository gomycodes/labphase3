import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';
import AddProduct from './AddProduct';
import ListProduct from './ListProduct';
function Product() {
  return (
    <Tabs
      defaultActiveKey="list"
      id="uncontrolled-tab-example"
      className="mb-3"
    >
      <Tab eventKey="list" title="Liste">
        <ListProduct />
      </Tab>
      <Tab eventKey="add" title="Ajouter">
        <AddProduct />
      </Tab>
    </Tabs>
  );
}

export default Product;