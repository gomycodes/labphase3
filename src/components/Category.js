import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';
import AddCategory from './AddCategory';
import ListCategory from './ListCategory';

function Category() {
  return (
    <Tabs
      defaultActiveKey="list"
      id="uncontrolled-tab-example"
      className="mb-3"
    >
      <Tab eventKey="list" title="Liste">
        <ListCategory />
      </Tab>
      <Tab eventKey="add" title="Ajouter">
        <AddCategory />
      </Tab>
    </Tabs>
  );
}

export default Category;