import logo from './logo.svg';
import './App.css';
import HeaderLayout from './layouts/HeaderLayout';
import SideBarLayout from './layouts/SideBarLayout';
import Product from './components/Product';
import Category from './components/Category';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Home from './components/Home';
import {Routes, Route } from "react-router-dom";
import { useLocation } from 'react-router-dom';
import { useState } from 'react';
import { useEffect } from 'react';
function App() {
  const location = useLocation()

  return (
    <div className="Apps">
      <HeaderLayout></HeaderLayout>

      {
      (location.pathname === '/' || location.pathname === '/home')?
       (<Row>
         <Home></Home>
        </Row>
       )
       : 
       (<Row>
        <Col xs={4} sm={4} md={3} lg={2} >
        <SideBarLayout></SideBarLayout>
        </Col>
        <Col xs={8} sm={8} md={9} lg={10} >
        <Routes>
        <Route path="/">
          <Route index element={<Home />} />
          <Route path="home" element={<Home />} />
          <Route path="categories" element={<Category />} />
          <Route path="products" element={<Product />} />
          <Route path="*" element={<Home />} />
        </Route>
        </Routes>
        </Col>
      </Row>)
      }
      
     

      
    </div>
  );
}

export default App;
