import React from 'react'
import ListGroup from 'react-bootstrap/ListGroup';
import { Outlet, Link } from "react-router-dom";

export default function SideBarLayout() {
  return (
    <div>
    <ListGroup>
      <ListGroup.Item>
       <Link to="/categories">Category</Link>
      </ListGroup.Item>
      <ListGroup.Item>
      <Link to="/products">Product</Link>
      </ListGroup.Item>
    </ListGroup>
    </div>
  )
}
